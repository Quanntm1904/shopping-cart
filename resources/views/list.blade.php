

@extends('layouts.app')
@section('title','Product list')
@section('content')
    <div class="container products">
        <div class="row">
            @foreach($products as $product)
                @php
                //dd($product);
                @endphp
                <div class="col-xs-18 col-sm-6 col-md-3">
                    <div class="thumbnail">
                        <img src="{{ $product->photo }}" alt="" height="300px" max-width="100%" />
                        <div class="caption">
                            <h4>{{ $product->name }}</h4>
                            <p>{{ Str::limit(Str::lower($product->description),50) }}</p>
                            <p><strong>Price: </strong> {{ $product->price }}</p>
                            <p class="btn-holder"><a href="{{ url('add-to-cart/'.$product->id) }}"
                                                     class="btn btn-warning btn-block text-center"
                                                     role="button">Add to cart</a>
                            </p>
                        </div>
                    </div>
                </div>
            @endforeach

        </div>
        {{--        End row--}}
    </div>

@endsection
