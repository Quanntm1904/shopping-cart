<?php

namespace App\Http\Controllers;

use App\Models\Product;
use Illuminate\Http\Request;
use Session;

class CartController extends Controller
{
    public function cart(){
        if (Session::has('cart')){
            $products = array();
            foreach (session('cart') as $id => $details){
                $product = Product::find($id);
                $products[$id] =[
                    'name' => $product->name,
                    'photo'=>$product->photo,
                    'price' => $product->price,
                ];
            }
        }
        return view('cart',compact('products'));
    }

    public function addToCart($id){
        $product = Product::find($id);

        if (!$product){
            abort(404);
        }

        $cart = session()->get('cart');


//        If the cart is empty
        if (!$cart){
            $cart = [
                $id => 1
            ];

            session()->put('cart', $cart);
            return redirect('/')->with('success','Product added to cart successfully!');
        }

//        If the product is already existing in the cart
        if (isset($cart[$id])){
            $cart[$id]++;
            session()->put('cart',$cart);

            return redirect('/')->with('success','product added to cart successfully!');
        }

        $cart[$id]=1;

        session()->put('cart', $cart);
        return redirect('/')->with('success','Product added to cart successfully');
    }

    public function update(Request $request){
        if ($request->id and $request->quantity){
            $cart = session()->get('cart');
            $cart[$request->id] = $request->quantity;
            session()->put('cart', $cart);
            session()->flash('success','Cart updated successfully!');
        }
    }

    public function remove (Request $request){
        $cart = session()->get('cart');
        if (isset($cart[$request->id])){
            unset($cart[$request->id]);
            session()->put('cart', $cart);
        }

        session()->flash('success','Product removed successfully!');
    }
}
